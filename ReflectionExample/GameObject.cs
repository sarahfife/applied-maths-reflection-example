﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace ReflectionExample
{
    class GameObject
    {
        private Model model;
        private Matrix[] transforms;

        public Vector3 position;
        public Vector3 scale = Vector3.One;
        public Vector3 rotation;

        public Vector3 collisionScale = Vector3.One;
        public Vector3 collisionOffset = Vector3.Zero;

        public bool colliding = false;

        // Behaviour

        public void LoadModel(ContentManager content, string name)
        {
            model = content.Load<Model>(name);

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
        }

        public void Draw()
        {
            // Create matrices to help with drawing
            Matrix view = Matrix.CreateLookAt(
                        new Vector3(0, 400, 10),
                        Vector3.Zero, // target
                        Vector3.Up
                        );

            // Perspective
            Matrix projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(45),
                16f / 9f,
                1f,
                100000f);

            // Loop through the meshes in the 3D model, drawing each one in turn
            foreach (ModelMesh mesh in model.Meshes)
            {
                // Loop through each effect on the mesh
                foreach (BasicEffect effect in mesh.Effects)
                {
                    // To render we need three things - world matrix, view matrix, and projection matrix
                    // But we actually start in model space - this is where our world starts before transforms

                    // ------------------------------
                    // MESH BASE MATRIX
                    // ------------------------------
                    // Our meshes start with world = model space, so we use our transforms array
                    effect.World = transforms[mesh.ParentBone.Index];


                    // ------------------------------
                    // WORLD MATRIX
                    // ------------------------------
                    // Transform from model space to world space in order - scale, rotation, translation.

                    // Scale
                    // Scale our model by multiplying the world matrix by a scale matrix
                    // XNA does this for use using CreateScale()
                    effect.World *= Matrix.CreateScale(scale);

                    // Rotation
                    // Rotate our model in the game world
                    effect.World *= Matrix.CreateRotationX(rotation.X); // Rotate around the x axis
                    effect.World *= Matrix.CreateRotationY(rotation.Y); // Rotate around the y axis
                    effect.World *= Matrix.CreateRotationZ(rotation.Z); // Rotate around the z axis

                    // Translation / position
                    // Move our model to the correct place in the game world
                    effect.World *= Matrix.CreateTranslation(position);

                    // ------------------------------
                    // VIEW MATRIX
                    // ------------------------------
                    // This puts the model in relation to where our camera is, and the direction of our camera.
                    effect.View = view;

                    // ------------------------------
                    // PROJECTION MATRIX
                    // ------------------------------
                    // Projection changes from view space (3D) to screen space (2D)
                    // Can be either orthographic or perspective

                    // Perspective
                    effect.Projection = projection;

                    // Orthographic
                    //effect.Projection = Matrix.CreateOrthographic(
                    //    1600, 900, 1f, 10000f
                    //    );

                    // ------------------------------
                    // LIGHTING
                    // ------------------------------
                    // Some basic lighting, feel free to tweak and experiment
                    effect.LightingEnabled = true;
                    effect.Alpha = 1.0f;
                    effect.AmbientLightColor = new Vector3(1.0f);
                }

                // Draw the current mesh using the effects we set up
                mesh.Draw();
            }
        }

        public BoundingSphere GetBoundingSphere()
        {

            BoundingSphere bounds = new BoundingSphere();

            // Use the position of our object + the center of the mesh
            bounds.Center = model.Meshes[0].BoundingSphere.Center + position + collisionOffset;

            // Scale our radius based on the model, our gameObject scale, and our collision scale
            // Just use X scale, as collision spheres are the same in all directions
            bounds.Radius = model.Meshes[0].BoundingSphere.Radius * scale.X * collisionScale.X;


            return bounds;
        }

        public BoundingBox GetFixedAABB()
        {

            BoundingBox bounds = new BoundingBox();

            // Set up the edges of our box as if we are at point 0
            // Size of fixed box is based on collisionScale 
            bounds.Min -= collisionScale * scale * 0.5f - new Vector3(model.Meshes[0].BoundingSphere.Radius);
            bounds.Max += collisionScale * scale * 0.5f + new Vector3(model.Meshes[0].BoundingSphere.Radius);

            // Move our box to the correct place in the world
            bounds.Min += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;
            bounds.Max += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;

            return bounds;
        }

        public BoundingBox GetDynamicAABB()
        {
            // Axis aligned bounding box
            BoundingBox hitBox = new BoundingBox();

            foreach (ModelMesh mesh in model.Meshes) // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    // Create an array to store the vertex data.
                    VertexPositionNormalTexture[] modelVertices = new VertexPositionNormalTexture[meshPart.VertexBuffer.VertexCount];

                    // Get the model's vertices
                    meshPart.VertexBuffer.GetData(modelVertices);

                    // Create a new array to store the position of each vertex.
                    Vector3[] vertices = new Vector3[modelVertices.Length];

                    // Get the bone transform
                    Matrix meshTransform = mesh.ParentBone.Transform;
                    meshTransform *= Matrix.CreateScale(scale); // scale  the mesh to the right size
                    meshTransform *= Matrix.CreateScale(collisionScale); // scale the mesh by the collision scale
                    meshTransform *= Matrix.CreateRotationX(rotation.X); // rotate the mesh
                    meshTransform *= Matrix.CreateRotationY(rotation.Y); // rotate the mesh
                    meshTransform *= Matrix.CreateRotationZ(rotation.Z); // rotate the mesh

                    // Loop throught the vertices.
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        // Get the position of the vertex
                        // Transform it using the bone transform
                        vertices[i] = Vector3.Transform(modelVertices[i].Position, meshTransform);
                    }

                    // Create a AABB from the model's vertices
                    hitBox = BoundingBox.CreateMerged(hitBox, BoundingBox.CreateFromPoints(vertices));
                }
            }

            // Move our box to the correct place in the world
            hitBox.Min += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;
            hitBox.Max += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;

            return hitBox;
        }

    }
}
