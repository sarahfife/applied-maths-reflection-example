﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ReflectionExample
{
    class BasicCuboid : BasicShape
    {
        private BoundingBox hitBox;
        private Vector3 scale;
        private Vector3 position;

        // ------------------
        // Behaviour
        // ------------------
        public BasicCuboid(GraphicsDevice graphics) : base(graphics) { }
        // ------------------
        public void SetUpVertices(Vector3 newPosition, Vector3 newScale)
        {
            scale = newScale;
            List<VertexPositionNormalTexture> verticesList = new
            List<VertexPositionNormalTexture>();
            //front
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 0), new
            Vector3(1, 0, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 0), new
            Vector3(0, 0, 1), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 0), new
            Vector3(0, 1, 0), new Vector2(1, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 0), new
            Vector3(1, 0, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 0), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 0), new
            Vector3(0, 1, 0), new Vector2(1, 0)));
            //back
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 1), new
            Vector3(0, 1, 0), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 1), new
            Vector3(0, 0, 1), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 1), new
            Vector3(1, 0, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 1), new
            Vector3(0, 1, 0), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 1), new
            Vector3(0, 0, 1), new Vector2(1, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 1), new
            Vector3(1, 0, 0), new Vector2(1, 1)));
            //left
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 0), new
            Vector3(0, 1, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 1), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 0), new
            Vector3(1, 0, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 0), new
            Vector3(0, 1, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 1), new
            Vector3(0, 0, 1), new Vector2(1, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 1), new
            Vector3(1, 0, 0), new Vector2(0, 0)));
            //right
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 0), new
            Vector3(0, 1, 0), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 0), new
            Vector3(1, 0, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 1), new
            Vector3(0, 0, 1), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 0), new
            Vector3(0, 1, 0), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 1), new
            Vector3(1, 0, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 1), new
            Vector3(0, 0, 1), new Vector2(1, 0)));
            //top
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 0), new
            Vector3(0, 1, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 0), new
            Vector3(1, 0, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 1), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 0), new
            Vector3(0, 1, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 1, 1), new
            Vector3(1, 0, 0), new Vector2(1, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 1, 1), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            //bottom
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 0), new
            Vector3(0, 1, 0), new Vector2(0, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 1), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 0), new
            Vector3(1, 0, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 0), new
            Vector3(0, 1, 0), new Vector2(1, 1)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(0, 0, 1), new
            Vector3(0, 0, 1), new Vector2(0, 0)));
            verticesList.Add(new VertexPositionNormalTexture(new Vector3(1, 0, 1), new
            Vector3(1, 0, 0), new Vector2(1, 0)));
            for (int vert = 0; vert < verticesList.Count; vert++)
            {
                VertexPositionNormalTexture temp = verticesList[vert];
                temp.Position *= scale;
                temp.Position += newPosition;
                verticesList[vert] = temp;
            }
            hitBox = new BoundingBox(newPosition, (newPosition + scale));
            vertexBuffer = new VertexBuffer(device,
            typeof(VertexPositionNormalTexture), verticesList.Count, BufferUsage.WriteOnly);
            vertexBuffer.SetData(verticesList.ToArray());
            numberOfPrimitives = verticesList.Count;
            primitiveType = PrimitiveType.TriangleList;

            // Setup physics
            position = newPosition;
        }
        // ------------------
        public BoundingBox GetBoundingBox()
        {
            return hitBox;
        }
        // ------------------
    }
}
