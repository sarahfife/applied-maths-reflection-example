﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ReflectionExample
{
    class BasicShape
    {
        // ------------------
        // Data
        // ------------------
        protected GraphicsDevice device;
        protected BasicEffect basicEffect;
        protected Texture2D faceTexture;
        protected VertexBuffer vertexBuffer;
        protected int numberOfPrimitives;
        protected PrimitiveType primitiveType;

        // ------------------
        // Behaviour
        // ------------------
        protected BasicShape()
        {
        } // exists purely to simplify the creation of subclasses
        // ------------------
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        // ------------------
        public BasicShape(GraphicsDevice graphicsDevice)
        {
            device = graphicsDevice;
            basicEffect = new BasicEffect(device);
            SetUpVertices();
        }
        // ------------------
        public void LoadContent(ContentManager content, string assetname)
        {
            faceTexture = content.Load<Texture2D>(assetname);
            basicEffect.Texture = faceTexture;
            basicEffect.TextureEnabled = true;
        }
        // ------------------
        public virtual void SetUpVertices()
        {
            numberOfPrimitives = 0;
        }
        // ------------------
        public virtual void Draw(Matrix viewMatrix, Matrix projectionMatrix)
        {
            basicEffect.World = Matrix.Identity;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.SetVertexBuffer(vertexBuffer);
                device.DrawPrimitives(primitiveType, 0, numberOfPrimitives);
            }
        }
        // ------------------
        public virtual void Draw(Matrix viewMatrix, Matrix projectionMatrix, Effect effect)
        {
            effect.CurrentTechnique = effect.Techniques["BasicEffect_Texture"];
            effect.Parameters["World"].SetValue(Matrix.Identity);
            effect.Parameters["WorldInverseTranspose"].SetValue(viewMatrix);
            effect.Parameters["WorldViewProj"].SetValue(projectionMatrix);
            effect.Parameters["Texture"].SetValue(faceTexture);
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                device.SetVertexBuffer(vertexBuffer);
                device.DrawPrimitives(primitiveType, 0, numberOfPrimitives);
            }
        }
        // ------------------
        public void Draw()
        {
            // Create matrices to help with drawing
            Matrix view = Matrix.CreateLookAt(
                        new Vector3(0, 400, 10),
                        Vector3.Zero, // target
                        Vector3.Up
                        );

            // Perspective
            Matrix projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(45),
                16f / 9f,
                1f,
                100000f);

            Draw(view, projection);
        }
    }
}
