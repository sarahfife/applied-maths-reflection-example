﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace ReflectionExample
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int displayWidth = 1080;
        int displayHeight = 720;

        BasicCuboid[] walls;
        GameObject ball = new GameObject();
       
        Vector3 ballVelocity = Vector3.Zero;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = displayWidth;
            graphics.PreferredBackBufferHeight = displayHeight;
            graphics.ApplyChanges();

            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Setup walls
            walls = new BasicCuboid[4];
            float wallLength = 200f;
            float halfWallLength = wallLength * 0.5f;
            for (int c = 0; c < walls.Length; c++)
            {
                walls[c] = new BasicCuboid(GraphicsDevice);
                walls[c].LoadContent(Content, "WallTexture");
            }
            walls[0].SetUpVertices(new Vector3(halfWallLength, -1, -halfWallLength), new Vector3(5, 30, wallLength));
            walls[1].SetUpVertices(new Vector3(-halfWallLength, -1, halfWallLength), new Vector3(wallLength, 30, 5));
            walls[2].SetUpVertices(new Vector3(-halfWallLength, -1, -halfWallLength), new Vector3(5, 30, wallLength));
            walls[3].SetUpVertices(new Vector3(-halfWallLength, -1, -halfWallLength), new Vector3(wallLength, 30, 5));

            // Setup ball
            ball.LoadModel(Content, "Ball");
            ball.scale = new Vector3(4);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            

            // Check if the player has clicked
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                // Get the mouse position on the screen;
                Point mousePos = Mouse.GetState().Position;

                // Subtract half the screen width and height
                // to put the mouse position relative to the screen's centre
                Vector2 relativeMousePos = new Vector2(mousePos.X - displayWidth / 2, 
                    mousePos.Y - displayHeight / 2);

                // Normalise this to make it length 1
                // so that no matter where we click on the screen
                // our ball gets the same amount of push in the
                // respective direction
                relativeMousePos.Normalize();

                // Put this relative mouse position in terms of 3D space 
                // along the horizontal plane
                Vector3 newVelocity = new Vector3(relativeMousePos.X, 0, relativeMousePos.Y);

                // Scale up to make the movement more noticeable
                newVelocity *= 300f;

                // Override the current velocity with this new one
                ballVelocity = newVelocity;
            }

            // Determine how far we are going to move
            // Multiply our velocity (game world distance / seconds) 
            // by the time passed this frame (seconds)
            Vector3 distanceToMove = ballVelocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Move the ball for this frame
            ball.position += distanceToMove;

            // Apply a drag to the ball's velocity
            // slowing it down for the next frame
            ballVelocity *= 0.99f;

            // Check for collisions
            for (int i = 0; i < walls.Length; ++i)
            {
                // If the current wall is intersecting our ball
                if (walls[i].GetBoundingBox().Intersects(ball.GetDynamicAABB()))
                {
                    // Move back to the position before we collided
                    ball.position -= distanceToMove;

                    // We need some vectors to keep track of our collision information
                    // We need two vectors on the colliding plane of the bounding box we ran into
                    // and the normal vector (perpendicular to the plane that we collided with)
                    Vector3 firstAxis, secondAxis, normal;

                    // Get our bounding boxes for easy access
                    BoundingBox thisHitBox = ball.GetDynamicAABB();
                    BoundingBox otherHitBox = walls[i].GetBoundingBox();

                    //---------------------------------------------------------
                    // Claculate collision depth

                    // Distance between the centers of Player and Other, on the X and Y coordinate
                    Vector3 centerOther = otherHitBox.Min + (otherHitBox.Max - otherHitBox.Min) / 2.0f;
                    Vector3 centerThis = thisHitBox.Min + (thisHitBox.Max - thisHitBox.Min) / 2.0f;
                    Vector3 distance = centerOther - centerThis;

                    // Minimum distance these need to be to NOT intersect
                    // AKA if they are AT LEAST this far away in either direction, they are not touching.
                    Vector3 minDistance = (otherHitBox.Max - otherHitBox.Min) / 2.0f + (thisHitBox.Max - thisHitBox.Min) / 2.0f;
                    if (distance.X < 0)
                        minDistance.X = -minDistance.X;
                    if (distance.Y < 0)
                        minDistance.Y = -minDistance.Y;
                    if (distance.Z < 0)
                        minDistance.Z = -minDistance.Z;

                    // Calculate and return intersection depths.
                    // Essentially, how much over the minimum intersection distance are we in each direction
                    // AKA by how much are they intersection in that direction?
                    Vector3 depth = minDistance - distance;

                    //---------------------------------------------------------

                    // Get the corners of the box in an array
                    // 0-3 are the south wall (in clockwise order from the top left, when facing that side)
                    // 4-7 are the north wall (in clockwise order from the top left, when facing that side)
                    Vector3[] corners = otherHitBox.GetCorners();

                    // Smallest depth = direction of collision
                    if (Math.Abs(depth.Z) > Math.Abs(depth.X))
                    {
                        // Colliding in the X direction (smaller depth)

                        // use the WEST plane for collision (east and west would be same)
                        firstAxis = corners[0] - corners[5]; // vector from the front top left to the back top left
                        secondAxis = corners[6] - corners[5]; // vector from the back bottom left to the back top left
                    }
                    else
                    {
                        // Colliding in the Z direction (smaller depth)

                        // use the SOUTH plane for collision (north would be the same)
                        firstAxis = corners[1] - corners[0]; // front top left to front top right
                        secondAxis = corners[3] - corners[0]; // front top left to front bottom left
                    }

                    // Get a cross product between those two vectors to define
                    // a normal (perpendicular) vector to the plane of collision
                    normal = Vector3.Cross(firstAxis, secondAxis);

                    // Reduce it to a unit vector (length 1)
                    normal.Normalize();

                    // Reflect the player's velocity off the surface using the normal
                    // This uses the dot product internally
                    ballVelocity = Vector3.Reflect(ballVelocity, normal);
                    
                }
            }

            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Draw the walls
            for (int c = 0; c < walls.Length; c++)
            {
                walls[c].Draw();
            }

            // Draw the ball
            ball.Draw();

            base.Draw(gameTime);
        }
    }
}
